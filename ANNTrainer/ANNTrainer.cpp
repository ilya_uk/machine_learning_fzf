#include <iostream>
#include <ANN.h>
#include <string>
using namespace std;
using namespace ANN;

const string input_file = "../data.dat";
const string output_file = "../network.dat";

int main()
{
	//cout << "hello ANN!" << endl;
	//cout << GetTestString().c_str() << endl;

	std::vector<std::vector<float>> in;
	std::vector<std::vector<float>> out;

	LoadData(input_file, in, out); //������ ������ �� �����

	vector < size_t > config({ 2, 10, 10, 1 });  //������ ��������� ����

	auto network = CreateNeuralNetwork(config, ANeuralNetwork::POSITIVE_SYGMOID);
	BackPropTraining(network, in, out, 100000, 0.1, 0.1, true);  //��������

	cout << "Information about network:" << endl;
	cout << network->GetType() << endl;				//����� ���������� � ��������� ����

	network->Save(output_file);			//���������� ��������� ���� � txt

	system("pause");
	return 0;
}