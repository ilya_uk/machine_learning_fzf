#include <iostream>
#include <ANN.h>
#include <string>
using namespace std;
using namespace ANN;


const string input_file = "../data.dat";
const string network_file = "../network.dat";

template < typename T >
std::ostream & operator << (std::ostream & out, std::vector < T > v)
{
	out << "(";
	for (size_t i = 0; i < v.size(); ++i)
	{
		out << v[i];
		if ((i + 1) != v.size())
		{
			out << ", ";
		}
	}
	out << ")";
	return out;
}

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

	auto network = CreateNeuralNetwork();
	network->Load(network_file);   //������ ��������� ���� �� �����

	cout << "Information about network:" << endl;
	cout << network->GetType() << endl;  //���������� � ���� ����

	std::vector<std::vector<float>> in;
	std::vector<std::vector<float>> out;

	LoadData(input_file, in, out); //������ ��������� �����

	for (int i = 0; i < in.size(); ++i)
	{ //����� ���� �� ���� ��������� ������
		auto out = network->Predict(in[i]);
		cout << endl;
		cout << "in:    " << in[i] << endl;
		cout << "out: " << out[i] << endl;
	//	cout << "out_my:   " << out_my << endl;
	}


	return 0;
	return 0;
}