#include "My_ANN.h"


#define ANNDLL_EXPORTS
#include "My_ANN.h"


My_ANN::My_ANN(vector<size_t> Configuration, ActivationType Activation_type)
{
	configuration = Configuration;
	activation_type = Activation_type;
}


My_ANN::~My_ANN()
{
}

string My_ANN::GetType()
{
	return "Neural Network by Borisuyk Ilya";
}

vector<float> My_ANN::Predict(std::vector<float> & input)
{
	vector<float> output;
	vector<float> prev = input;
	for (int i = 1; i < configuration.size(); i++)
	{
		output.clear();
		for (int j = 0; j < configuration[i]; j++)
		{
			float d = 0;
			for (int k = 0; k < configuration[i - 1]; k++)
			{
				d += weights[i - 1][j][k] * prev[k];
			}
			output.push_back(Activation(d));
		}
		prev = output;
	}

	return output;
}


float ANN::BackPropTraining
(
std::shared_ptr<ANN::ANeuralNetwork>ann,
std::vector < std::vector < float > > & inputs,
std::vector < std::vector < float > > & outputs,
int max_iters,
float eps,
float speed,
bool std_dump
)
{
	std::vector < std::vector < std::vector < float > > > weight_deltas;//���

	ann->RandomInit();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	weight_deltas.resize(ann->configuration.size() - 1);

	for (int i = 0; i < ann->weights.size(); i++)
	{
		weight_deltas[i].resize(ann->configuration[i + 1]);

		for (int j = 0; j < ann->weights[i].size(); j++)
		{
			weight_deltas[i][j].resize(ann->configuration[i]);

		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////


	float err;
	int iterations = 0;


	return err;

}



std::shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(
	std::vector<size_t> & configuration,
	ANeuralNetwork::ActivationType activation_type, float scale)
{
	return std::shared_ptr<ANeuralNetwork>(new My_ANN(configuration, activation_type));
}