#pragma once
#include "ANN.h"
#include <time.h>
#include <iostream>

using namespace ANN;
using namespace std;

class My_ANN:
	public ANeuralNetwork
{
public:
	My_ANN(vector<size_t> configuration, ActivationType activation_type);
	~My_ANN();
	string GetType();
	vector<float> Predict(std::vector<float> & input);
	float BackPropTraining(
		std::vector<std::vector<float>> & inputs,
		std::vector<std::vector<float>> & outputs,
		int max_iters = 10000,
		float eps = 0.1,
		float speed = 0.1,
		bool std_dump = false
		);
};


